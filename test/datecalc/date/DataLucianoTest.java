/*
 * Não foi tratado os teste de validação
 */
package datecalc.date;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 * Teste passado para seleção de vaga
 * @author luciano
 */
public class DataLucianoTest {

//    DataLuciano instance = new DataLuciano();
    CalcDias instance = new CalcDias();

    public DataLucianoTest() {
    }

    /**
     * Test of datas method, of class DataLuciano.
     */
    @Test
    public void test01Datas() {
        System.out.println("Teste 1");
        String dataInicio = "2018-01-01";
        String dataTermino = "2018-01-02";
        int expResult = 1;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test02Datas() {
        System.out.println("Teste 2");
        String dataInicio = "2018-01-01";
        String dataTermino = "2018-02-01";
        int expResult = 31;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test03Datas() {
        System.out.println("Teste 3");
        String dataInicio = "2018-01-01";
        String dataTermino = "2018-02-02";
        int expResult = 32;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test04Datas() {
        System.out.println("Teste 4");
        String dataInicio = "2018-01-01";
        String dataTermino = "2018-02-28";
        int expResult = 58;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test05Datas() {
        System.out.println("Teste 5");
        String dataInicio = "2018-01-15";
        String dataTermino = "2018-03-15";
        int expResult = 59;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test06Datas() {
        System.out.println("Teste 6");
        String dataInicio = "2018-01-01";
        String dataTermino = "2019-01-01";
        int expResult = 365;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test07Datas() {
        System.out.println("Teste 7");
        String dataInicio = "2018-01-01";
        String dataTermino = "2020-01-01";
        int expResult = 730;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test08Datas() {
        System.out.println("Teste 8");
        String dataInicio = "2018-12-31";
        String dataTermino = "2019-01-01";
        int expResult = 1;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test09Datas() {
        System.out.println("Teste 9");
        String dataInicio = "2018-05-31";
        String dataTermino = "2018-06-01";
        int expResult = 1;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test10Datas() {
        System.out.println("Teste 10");
        String dataInicio = "2018-05-31";
        String dataTermino = "2019-06-01";
        int expResult = 366;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test11Datas() {
        System.out.println("Teste 11");
        String dataInicio = "2016-02-01";
        String dataTermino = "2016-03-01";
        int expResult = 29;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test12Datas() {
        System.out.println("Teste 12");
        String dataInicio = "2016-01-01";
        String dataTermino = "2016-03-01";
        int expResult = 60;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test13Datas() {
        System.out.println("Teste 13");
        String dataInicio = "1981-09-21";
        String dataTermino = "2009-02-12";
        int expResult = 10006;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }

    @Test
    public void test14Datas() {
        System.out.println("Teste 14");
        String dataInicio = "1981-07-31";
        String dataTermino = "2009-02-12";
        int expResult = 10058;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test15Datas() {
        System.out.println("Teste 15");
        String dataInicio = "2004-03-01";
        String dataTermino = "2009-02-12";
        int expResult = 1809;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test16Datas() {
        System.out.println("Teste 16");
        String dataInicio = "1900-02-01";
        String dataTermino = "1900-03-01";
        int expResult = 28;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test17Datas() {
        System.out.println("Teste 17");
        String dataInicio = "1899-01-01";
        String dataTermino = "1901-01-01";
        int expResult = 730;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test18Datas() {
        System.out.println("Teste 18");
        String dataInicio = "2000-02-01";
        String dataTermino = "2000-03-01";
        int expResult = 29;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }
    
    @Test
    public void test19Datas() {
        System.out.println("Teste 18");
        String dataInicio = "1999-01-01";
        String dataTermino = "2001-01-01";
        int expResult = 731;
        int result = instance.datas(dataInicio, dataTermino);
        assertEquals(expResult, result);
    }
}
