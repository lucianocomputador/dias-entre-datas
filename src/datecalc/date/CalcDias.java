/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datecalc.date;

/**
 *
 * @author luciano
 */
public class CalcDias {

    static class Date {

        int dia, mes, ano;

        public Date(int dia, int mes, int ano) {
            this.dia = dia;
            this.mes = mes;
            this.ano = ano;
        }
    };

    /**
     * Retorna quantidade de dias dos meses do ano
     */
    static int[] diasDoMes = {31, 28, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31};

    /**
     * Conta quantos anos bissextos tem entre as datas
     *
     * @param data
     * @return
     */
    static int quantosAnosBissextos(Date data) {
        int anos = data.ano;
        if (data.mes <= 2) {
            anos--;
        }
        return anos / 4 - anos / 100 + anos / 400;
    }

    /**
     * Retorna o quantos dias tem entre as datas
     *
     * @param dt1dataInicial@param dataFinal
     * @return
     */
    static int getQtdDias(Date dataInicial, Date dataFinal) {
        // Contar quantos dias tem no ano inicial
        int n1 = dataInicial.ano * 365 + dataInicial.dia;

        // Adiciona dias dos meses do ano inicial
        for (int i = 0; i < dataInicial.mes - 1; i++) {
            n1 += diasDoMes[i];
        }

        // Adiciona 1 dia para cada ano bissexto
        n1 += quantosAnosBissextos(dataInicial);

        // Repetir operações na data iniical para data final
        int n2 = dataFinal.ano * 365 + dataFinal.dia;
        for (int i = 0; i < dataFinal.mes - 1; i++) {
            n2 += diasDoMes[i];
        }
        n2 += quantosAnosBissextos(dataFinal);

        // Retorna a diferença da contagem que é q quantidade de dias
        return (n2 - n1);
    }

    /**
     * Metodo para chamar o calculo dos dias entre datas
     *
     * @param inicio
     * @param fim
     * @return
     */
    public int datas(String inicio, String fim) {
        try {
            DataToNumero dataInicial = new DataToNumero(inicio);
            Date dt1 = new Date(dataInicial.getDia(), dataInicial.getMes(), dataInicial.getAno());
            DataToNumero dataFinal = new DataToNumero(fim);
            Date dt2 = new Date(dataFinal.getDia(), dataFinal.getMes(), dataFinal.getAno());
            // Retorno quantidade de dias
            return getQtdDias(dt1, dt2);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    /**
     * Classe para converte data tipo string em números, Separadamente os
     * números de dia mes e ano
     */
    public class DataToNumero {

        private int dia;
        private int mes;
        private int ano;

        public int getDia() {
            return dia;
        }

        public int getMes() {
            return mes;
        }

        public int getAno() {
            return ano;
        }

        /**
         * Recebe data a ser convertida
         *
         * @param data
         */
        public DataToNumero(String data) throws Exception {
            String[] dataSeparada = data.split("-");
            try {
                dataValida(dataSeparada);
                this.dia = Integer.parseInt(dataSeparada[2]);
                this.mes = Integer.parseInt(dataSeparada[1]);
                this.ano = Integer.parseInt(dataSeparada[0]);
            } catch (Exception e) {
                throw new Exception(e.getMessage());
//                System.out.println(e.getMessage());
            }

        }

    }

    /**
     * Validada data
     *
     * @param data
     * @return
     * @throws Exception
     */
    private boolean dataValida(String[] data) throws Exception {
//        try {
        if (data.length == 3) {

            Integer dia;
            Integer mes;
            Integer ano;

            try {
                dia = Integer.parseInt(data[2]);
                mes = Integer.parseInt(data[1]);
                ano = Integer.parseInt(data[0]);
            } catch (NumberFormatException e) {
                throw new Exception("Data inválida. Deve tem apenas números");
            }

            if (dia < 1 || mes < 1 || ano < 1) {
                throw new Exception("Data inválida. Dia deve ser maior que 1");
            }

            if ((mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
                    && dia > 31) {
                throw new Exception("Data inválida. Os meses 1,3,5,7,9,10 e 12 têm 31 dias");
            }

            if ((mes == 4 || mes == 6 || mes == 9 || mes == 11)
                    && dia > 30) {
                throw new Exception("Data inválida. Os meses 4,6,9 e11 têm 30 dias");
            }

            if ((mes == 2 && isBissexto(ano))
                    && dia > 29) {
                throw new Exception("Data inválida. Mês 2 deste ano tem 29 dias");
            }

            if ((mes == 2 && !isBissexto(ano))
                    && dia > 28) {
                throw new Exception("Data inválida. Mês 2 deste ano tem 28 dias");
            }

        } else {
            throw new Exception("Data inválida. Aceitável dia/mes/ano");
        }
//        } catch (Exception e) {
//            throw new Exception(e.getMessage());
//        }

        return true;
    }

    /**
     * Verificar se o ano é bissexto
     *
     * @param ano
     * @return
     */
    private boolean isBissexto(Integer ano) {
        return (((ano % 4 == 0) && (ano % 100 > 0)) || (ano % 400 == 0));

    }

}
