/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datecalc;

import datecalc.date.CalcDias;
import java.util.Scanner;

/**
 *
 * @author luciano
 */
public class DateCalc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        testeManual();

    }

    public static void testeManual() {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Programa capaz de calcular o número de dias entre duas datas.\n");

        CalcDias calcula = new CalcDias();

        String entrada;

        System.out.println("Entre com a primeira data.. Exemplo. ano-mes-dia");
        entrada = myObj.nextLine();

        String dataInicial = entrada;

        System.out.println("Entre com a segunda data. Exemplo. ano-mes-dia");
        entrada = myObj.nextLine();

        String dataFinal = entrada;

        System.out.println(calcula.datas(dataInicial, dataFinal));

    }

 
}
